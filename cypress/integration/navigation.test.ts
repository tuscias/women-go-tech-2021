const expandNavigationMenu = () => {
  cy.get("nav[role=navigation]").contains("Company").click();
};

const clickNvigationMenuItem = (label: string) => {
  expandNavigationMenu();
  cy.get("li.menu-item-type-custom").contains(label).click();
};

describe("Navigation menu", () => {
  beforeEach(() => {
    cy.visit("https://geniussports.com");
  });

  it("should navigate to Company -> Investors", () => {
    expandNavigationMenu();
    //
    // More recipies on how to test links:
    // https://github.com/cypress-io/cypress-example-recipes/blob/master/examples/testing-dom__tab-handling-links/cypress/integration/tab_handling_anchor_links_spec.js
    //
    cy.get("li.menu-item-type-custom a")
      .contains("Investors")
      .should("have.prop", "href")
      .and("equal", "https://investors.geniussports.com/");
  });

  it("should navigate to Company -> About", () => {
    clickNvigationMenuItem("About");
  });

  it("should navigate to Company -> News", () => {
    expandNavigationMenu();

    cy.get("li.menu-item-type-custom a")
      .contains("News")
      .should("have.attr", "target", "_blank")
      .should("have.prop", "href")
      .and("equal", "http://news.geniussports.com/"); // <- shouldn't this have https:// ?
  });

  it("should navigate to Company -> Careers", () => {
    clickNvigationMenuItem("Careers");
    cy.url().should("eq", "https://geniussports.com/home/careers/");
  });

  it("should navigate to Company -> Partners", () => {
    clickNvigationMenuItem("Partners");
    cy.url().should("eq", "https://geniussports.com/home/partners/");
  });

  it("should navigate to Company -> Contact Us", () => {
    clickNvigationMenuItem("Contact Us");
    cy.url().should("eq", "https://geniussports.com/home/contact-us/");
  });
});
