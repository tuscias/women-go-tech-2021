describe("Company -> Careers", function () {
  describe("Sub-menu navigation", () => {
    before(() => {
      cy.visit("/home/careers/");
    });

    it("should navigate to Locations", () => {
      cy.get(".menu-item-object-custom").contains("Locations").click();
      cy.get("#locations").should("be.visible");
    });

    it("should navigate to Teams", () => {
      cy.get(".menu-item-object-custom").contains("Teams").click();
      cy.get("#teams").should("be.visible");
    });

    it("should navigate to Culture", () => {
      cy.get(".menu-item-object-custom").contains("Culture").click();
      cy.get("#culture").should("be.visible");
    });
  });

  describe("Openings", function () {
    const roleName = "Senior QA Engineer";
    const iframeId = "#grnhse_iframe";

    it("should navigate to Openings", () => {
      cy.get(".menu-item-object-custom").contains("Openings").click();
      cy.url().should("eq", "https://geniussports.com/home/careers/jobs/");
    });

    it("should show job opening filtered by Location", () => {
      cy.visit("/home/careers/jobs/");

      cy.frameLoaded(iframeId);
      cy.iframe(iframeId)
        .find("#s2id_offices-select")
        .should("be.visible")
        .click();

      cy.iframe(iframeId)
        .find("#select2List1 li")
        .contains("Vilnius")
        .scrollIntoView()
        .click();

      cy.iframe(iframeId)
        .find(".opening a")
        .contains(roleName)
        .should("have.attr", "target", "_top")
        .then((el) => {
          const link = el.attr("href") || "";
          expect(link).not.to.be.empty;
          cy.task("setItem", { name: "job-opening-link", value: link });
        });
    });

    it("should open job listing page from url", () => {
      cy.task("getItem", "job-opening-link").then((link) => {
        cy.visit(link as string);

        cy.get(".app-title").should("contain.text", roleName);
        cy.get("#apply_button").should("be.visible");
      });
    });
  });
});
