# women-go-tech-2021
Sample testsuite to show how tests are written using [Cypress](https://www.cypress.io/)

## prepare project
> npm install

## to open cypress
> npm run cypress:open

# to run tests
> npm run cypress:run

# to run tests in headless mode (on CI)
> npm run cypress:run:headless

# to make html report after test run
> npm run cypress:report

